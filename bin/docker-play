#!/bin/sh
set -e

{
# Custom groups, in order they should be applied
COMPOSE_STACK_FILE=".compose-stack"

# Array of compose files, we make sure the different groups (prototype, gateway
# and app) are in the right order regardless of what order the user passes them
# in by separating the array IDs for each type by 100
COMPOSE_FILE_GROUPS_SEPARATOR=100
# The main, required group is "" which covers the docker-compose.yml,
# docker-compose.override.yml and docker-compose.dev.*.yml
COMPOSE_FILE_GROUPS=
COMPOSE_HELP=false
# Debug config, pass -d or --dry-run to just print the compose command without
# executing it
COMPOSE_DRY_RUN=false

# compose_dev_slug $main_slug
compose_dev_slug() {
  # $main_slug is group name
  main_slug=$1
  # dev_slug is format of dev. compose files,
  # dev.* for root group and group.dev.* for other groups
  dev_slug="dev"
  if [ ! -z "${main_slug}" ]; then
    dev_slug="${main_slug}.${dev_slug}"
  fi
  echo "${dev_slug}"
}

# compose_load_stack
compose_load_stack() {
  # Load the stack file
  if [ ! -f "${COMPOSE_STACK_FILE}" ]; then
    echo "Error, could not find compose stack file: ${COMPOSE_STACK_FILE}"
    exit 101
  fi
  COMPOSE_STACK_FILE_CONTENTS=$(head -n 1 ${COMPOSE_STACK_FILE})
  COMPOSE_STACK_FILE_FIRST_CHAR=$(echo "${COMPOSE_STACK_FILE_CONTENTS}" | awk '{ string=substr($0, 1, 1); print string; }')
  if [ "${COMPOSE_STACK_FILE_FIRST_CHAR}" = ' ' ]; then
    COMPOSE_STACK_FILE_REMAINING=$(echo "${COMPOSE_STACK_FILE_CONTENTS}" | awk '{ string=substr($0, 2); print string; }')
    COMPOSE_CUSTOM_GROUPS=(${COMPOSE_STACK_FILE_REMAINING})
    COMPOSE_FILE_GROUPS=("" "${COMPOSE_CUSTOM_GROUPS[@]}")
  else
    COMPOSE_FILE_GROUPS=(${COMPOSE_STACK_FILE_CONTENTS})
  fi
  # Initialize the array of base indexes for each group (0, 100, 200)
  # and the array of counts for each group (0, 0, 0)
  i=0
  valid_slugs_count=0
  while [ "$i" -lt "${#COMPOSE_FILE_GROUPS[@]}" ]; do
    COMPOSE_FILE_GROUP_BASE_IDX["$i"]=$((i * COMPOSE_FILE_GROUPS_SEPARATOR))
    COMPOSE_FILE_GROUP_IDX["$i"]=0

    main_slug="${COMPOSE_FILE_GROUPS[$i]}"
    dev_slug=$(compose_dev_slug "${main_slug}")
    if [ ! -z "${main_slug}" ]; then
      VALID_SLUGS[$valid_slugs_count]="${main_slug}"
      valid_slugs_count=$((valid_slugs_count + 1))
    fi
    VALID_SLUGS[$valid_slugs_count]="${dev_slug}*"
    valid_slugs_count=$((valid_slugs_count + 1))

    i=$((i + 1))
  done
}

# compose_add_files_main $i
compose_add_files_main() {
  i=$1; only_this_group=$2;
  main_slug="${COMPOSE_FILE_GROUPS[$i]}"
  # If the group is "" (root), then the name for compose files is
  # docker-compose$main_slug where $main_slug is "", otherwise it is
  # docker-compose-$main_slug where $main slug is group name
  separator=""; if [ ! -z "${main_slug}" ]; then separator="-"; fi
  # The main compose files for a group should always be before the dev ones so
  # these are positioned starting from the first index, e.g. 0 for the root group
  # 0, 100 for the group 1, 200 for group 2
  compose_file="docker-compose${separator}${main_slug}.yml"
  if [ ! -f "${compose_file}" ]; then
    echo "Error, could not find compose file: ${compose_file}"
    exit 102
  fi
  COMPOSE_FILES["${COMPOSE_FILE_GROUP_BASE_IDX[$i]}"]="-f"
  COMPOSE_FILES["$((COMPOSE_FILE_GROUP_BASE_IDX[$i] + 1))"]="${compose_file}"
  compose_override_file="docker-compose${separator}${main_slug}.override.yml"
  if [ -f "${compose_override_file}" ]; then
    COMPOSE_FILES["$((COMPOSE_FILE_GROUP_BASE_IDX[$i] + 2))"]="-f"
    COMPOSE_FILES["$((COMPOSE_FILE_GROUP_BASE_IDX[$i] + 3))"]="${compose_override_file}"
  fi
  # Add group below this one
  if [ "${only_this_group}" != true ] && [ "$i" -gt 0 ]; then
    compose_add_files_main "$((i - 1))" "${only_this_group}"
  fi
}

# compose_add_files_dev $i $slug
compose_add_files_dev() {
  j=$1; slug=$2; only_this_group=$3;
  # $main_slug is group name
  # $slug is the user supplied dev slug dev.* or group.dev.*
  compose_add_files_main "$j" "${only_this_group}"
  # If the group is "" (root), then the name for dev compose files is
  # docker-compose.$slug where $slug is dev.*, otherwise it is
  # docker-compose-$slug where $slug is group.dev.*
  main_slug="${COMPOSE_FILE_GROUPS[$j]}"
  separator="."; if [ ! -z "${main_slug}" ]; then separator="-"; fi
  compose_dev_file="docker-compose${separator}${slug}.yml"
  if [ ! -f "${compose_dev_file}" ]; then
    echo "Error, could not find compose dev file: ${compose_dev_file}"
    exit 103
  fi
  COMPOSE_FILES["$((COMPOSE_FILE_GROUP_BASE_IDX[$j] + 4 + COMPOSE_FILE_GROUP_IDX[$j]))"]="-f"
  COMPOSE_FILES["$((COMPOSE_FILE_GROUP_BASE_IDX[$j] + 4 + COMPOSE_FILE_GROUP_IDX[$j] + 1))"]="${compose_dev_file}"
  COMPOSE_FILE_GROUP_IDX["$j"]=$((COMPOSE_FILE_GROUP_IDX[$j] + 2))
}

# compose_process $args
compose_process() {
  command_found=false
  file_opt_found=false
  stack_file_loaded=false
  args_idx=0
  while [ "$#" -gt 0 ]; do
    # Match args and find command (first arg that does not start with a "-")
    case "$1" in
    -*)
      if [ "${command_found}" != true ]; then
        # These are compose opts, and not opts for the compose command
        # so process them looking for any extra commands
        if [ "$1" = "-d" ] || [ "$1" = "--dry-run" ]; then
          COMPOSE_DRY_RUN=true
          shift
        elif [ "$1" = "-s" ] || [ "$1" = "--stack-file" ]; then
          shift
          if [ "${file_opt_found}" = true ]; then
            echo "Error, group or only group option specified"
            echo "before the -s, --stack-file STACK_FILE argument"
            echo "You must specify the stack file before any groups"
            exit 1
          fi
          COMPOSE_STACK_FILE="$1"
          if [ -z "${COMPOSE_STACK_FILE}" ]; then
            echo "Error, empty compose stack file name"
            echo "You must specify a valid file name, e.g. .compose-stack"
            exit 2
          fi
          shift
        elif [ "$1" = "-g" ] || [ "$1" = "--group" ] \
          || [ "$1" = "-o" ] || [ "$1" = "--only-group" ]; then
          file_opt_found=true
          if [ "${stack_file_loaded}" != true ]; then
            compose_load_stack
            stack_file_loaded=true
          fi
          only_this_group=false
          if [ "$1" = "-o" ] || [ "$1" = "--only-group" ]; then
            only_this_group=true
          fi
          shift
          slug=$1
          shift

          # Go through each group and see if it the main or dev slugs for that group
          # match the supplied compose file slug
          i=0
          match_found=false
          while [ "$i" -lt "${#COMPOSE_FILE_GROUPS[@]}" ]; do
            main_slug="${COMPOSE_FILE_GROUPS[$i]}"
            dev_slug=$(compose_dev_slug "${main_slug}")
            # Match the main slug, since the user cannot pass the root slug (empty)
            # it is required to manually add the main files for the root in
            # compose_initialize
            if [ "${slug}" = "${main_slug}" ]; then
              match_found=true
              compose_add_files_main "${i}" "${only_this_group}"
              break
            else
              # Try to match dev.* slug
              case "${slug}" in
              "${dev_slug}"*)
                match_found=true
                compose_add_files_dev "${i}" "${slug}" "${only_this_group}"
                break
                ;;
              *)
                ;;
              esac
            fi
            i=$((i + 1))
          done
          if [ "${match_found}" != true ]; then
            echo "Error, invalid compose file slug: ${slug}"
            echo "Valid slugs are: ${VALID_SLUGS[@]}"
            exit 3
          fi
        elif [ "$1" = "-p" ] || [ "$1" = "--project-name" ] \
          || [ "$1" = "-f" ] || [ "$1" = "--file-name" ]; then
          # These options have a value that comes after
          COMPOSE_ARGS["${args_idx}"]="$1"
          args_idx=$((args_idx + 1))
          shift
          value=$1
          if [ -z "${value}" ]; then
            echo "Error, empty value"
            exit 4
          fi
          COMPOSE_ARGS["${args_idx}"]="$1"
          args_idx=$((args_idx + 1))
          shift
        else
          if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
            COMPOSE_HELP=true
          fi
          COMPOSE_ARGS["${args_idx}"]="$1"
          args_idx=$((args_idx + 1))
          shift
        fi
      else
        COMPOSE_ARGS["${args_idx}"]="$1"
        args_idx=$((args_idx + 1))
        shift
      fi
      ;;
    *)
      if [ "${command_found}" != true ]; then
        command_found=true
        # Process command
        case "$1" in
        *)
          COMPOSE_ARGS["${args_idx}"]="$1"
          args_idx=$((args_idx + 1))
          shift
          ;;
        esac
      else
        COMPOSE_ARGS["${args_idx}"]="$1"
        args_idx=$((args_idx + 1))
        shift
      fi
      ;;
    esac
  done

  # If no groups or files were specified, load the root group in the stack
  if [ "${stack_file_loaded}" != true ] && [ "${COMPOSE_HELP}" != true ]; then
    compose_load_stack
    compose_add_files_main 0 true
  fi

  COMPOSE_COMMAND=("docker-compose" "${COMPOSE_FILES[@]}" "${COMPOSE_ARGS[@]}")
}

# compose_execute
compose_execute() {
  if [ "${COMPOSE_DRY_RUN}" = true ]; then
    # Print out the command
    echo "${COMPOSE_COMMAND[@]}"
  else
    if [ "${COMPOSE_HELP}" = true ]; then
      compose_help
    fi
    eval "${COMPOSE_COMMAND[@]}"
  fi
}

# compose_help
compose_help() {
  echo "Define and run large multi-container applications with Docker Compose."
  echo ""
  echo "Usage:"
  echo "  docker-play [-s=<arg>...] [options] [COMMAND] [ARGS...]"
  echo "  docker-play -h|--help"
  echo ""
  echo "Options:"
  echo "  -s, --stack-file FILE     Specify an alternate compose stack file (default: .compose-stack)"
  echo "  -g, --group-name NAME     Add the compose files for the group and lower groups in the stack"
  echo "  -o, --only-group NAME     Add the compose files for only this group"
  echo "  -d, --dry-run             Print compose command and exit"
  echo ""
}

# Process the docker-compose args
compose_process "$@"

# Execute the processed args
compose_execute
}
