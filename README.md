# Docker Play

The `docker-play` executable is a small wrapper script around `docker-compose` that makes it easier to work with multiple compose files.

With Docker Play, Compose files are divided into groups, which are ordered in a stack. A group is a collection of services that run together, for example micro-services for an API, the services that make up an API gateway or services for a front-end. Each group depends on those lower in the stack.

The first group is the root group, which is defined in the `docker-compose.yml` and `docker-compose.override.yml` (optional) files. Other groups, and their ordering are defined in a `.compose-stack` file. Services for each group are defined in `docker-compose-<group>.yml` and `docker-compose-<group>.override.yml` (optional) files.


## Getting starting

Install the latest `docker-play`:

    curl -L https://bitbucket.org/lenioapp/docker-play/raw/master/bin/docker-play > /usr/local/bin/docker-play
    chmod +x /usr/local/bin/docker-play

Create a `.compose-stack` file listing the all groups except the first in your stack, for instance if your project has a root api group (collection of API micro-services), a gateway group (an API gateway composed of a router, management dashboard and database) and a frontend group (web front-end) you would define the following groups in your `.compose-stack` file:

    api gateway frontend

Docker Play then expects your project to be split into compose files named as follows:

* `docker-compose-<group>.yml` (or `docker-compose.yml` if group name is empty) - main compose file, required
* `docker-compose-<group>.override.yml` (or `docker-compose.override.yml` if group name is empty) - default override, loaded with main file if it exists
* `docker-compose-<group>.dev<*>override.yml` (or `docker-compose.dev<*>override.yml` if group name is empty) - dev override for one more more components, e.g. running the components directly from source code

If you want the compose files for the first group to be named `docker-compose.yml` (and `docker-compose.override.yml`) then give the first group an empty name, e.g.:

     gateway frontend

See the example project for more information on working with Docker Play.


## Options


### -s, --stack-file FILE

Load an alternate stack file instead of `.compose-stack`. This option must be specified before any `-g` or `-o` options.


### -g, --group-name NAME

The `--group-name NAME` argument replaces the Compose `-f FILE` argument, where the name is worked out by removing `docker-compose.` or `docker-compose-` from the beginning of the file and `.yml` from the end, resulting in e.g. `dev.service1` for `docker-compose.dev.service1.yml` or `gateway` for `docker-compose-gateway.yml`.

The default `.override` file will be added for each group specified if it exists. When you add a group, any group lower in the stack is automatically added.

If you specify a dev variation such as `name.dev*` for any group then the main compose file and override for that group will be added for you, as will groups lower in the stack.

Unlike with the `docker-compose`, the file order is not important, the `docker-play` will ensure that the files are in the right order when passed to `docker-compose`.


### -o, --only-group NAME

The `--only-group NAME` argument allows you to run a groups without including groups lower in the stack. If you specify a dev variation, the main compose files for that group are still included.

You can still the use the `-f FILE` argument if you wish to specify a file that does not fit the pattern. These will appear after any files managed by `docker-play`.


### -d, --dry-run

The `--dry-run` argument prints the `docker-compose` that would be run, but does not execute it. This is useful for verifying or inspecting commands before running them.


### Other options

Except for the options mentioned above arguments are passed untouched to the `docker-compose` executable, letting you pass any command or option to compose such as `up -d` or `logs service1`.


## Example project

See the `example` directory for a simple project. Use the `--dry-run` option with this project to inspect how Docker Play works.

The example project has 3 stacks: the main one linking API, gateway and frontend, a test stack for the frontend which runs on a mock gateway, and a test stack for the gateway which runs on a mock API.

For instance to bring up the whole stack, you would run:

    $ docker-play -g frontend up -d

which would run:

    docker-compose -f docker-compose-api.yml -f docker-compose-api.override.yml -f docker-compose-gateway.yml -f docker-compose-frontend.yml up -d

Or to just start the frontend on it's own:

    $ docker-play -o frontend up -d

which would run:

    docker-compose -f docker-compose-frontend.yml up -d

To start the stack with `service1` running from the dev configuration:

    $ docker-play -g api.dev.service1 -g frontend up -d

which would run:

    docker-compose -f docker-compose-api.yml -f docker-compose-api.override.yml -f docker-compose-api.dev.service1.yml -f docker-compose-gateway.yml -f docker-compose-frontend.yml up -d  

To start the front-end with a mock gateway using the frontend test stack:

    $ docker-play -s .compose-stack.test.frontend -g frontend up -d

which would run:

    docker-compose -f docker-compose-gateway.test.yml -f docker-compose-frontend.yml up -d

Or to start the gateway using a mock API:

    $ docker-play -s .compose-stack.test.gateway -g gateway up -d

which would run:

    docker-compose -f docker-compose-api.test.yml -f docker-compose-gateway.yml up -d


## Scope of this project

The Docker Play project is intended to formalise common patterns in Compose projects and make it easier to work with Docker Compose, especially in larger projects.

For instance, the addition of new Compose commands (such as `downup` as a shortcut for `down` and then `up` or an `exec` command to match `docker exec`) are outside the scope of this project.
